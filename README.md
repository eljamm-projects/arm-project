Description
===
Mini project to flip LEDs using joystick interrupt services. The code (mix between C and ARM Assembly) was written and simulated in Keil uvision.

<p align="center">
	<img src="assets/ARM.png" alt="ARM"/>
</p>


Simulation
===
![Simulation Video](assets/Joystick-Interrupt.mp4)
